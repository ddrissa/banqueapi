package ci.kossovo.banque_api.api_write.commands.clients;

import java.util.UUID;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class AddClientCommand {

    @TargetAggregateIdentifier
    private final UUID id;
    private final String nom;
    private final String prenom;
}

package ci.kossovo.banque_api.api_write.commands.clients;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class UpdateClientCommand {

    private final UUID id;
    private final String nom;
    private final String prenom;
}
